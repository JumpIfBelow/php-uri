# PHP URI
A simple class defining what is an URI, and easy to transform into a string.

## How to install?
Use Composer as usual:
```bash
composer require jumpifbelow/php-uri
```

## What it looks like?
```php
<?php
use Uri\Uri;

$uri = new Uri();

$uri
    ->setScheme('https')
    ->setHost('jsonplaceholder.typicode.com')
    ->setPath('/posts/1')
;

// will contain https://jsonplaceholder.typicode.com/posts/1
$string = $uri->__toString();

// can change the port of the protocol
$uri->setPort(8000);

// can handle authentication
$uri
    ->setUsername('user')
    ->setPassword('password')
;

// will contain https://user:password@jsonplaceholder.typicode.com:8000/posts/1
$string = $uri->__toString();
```
