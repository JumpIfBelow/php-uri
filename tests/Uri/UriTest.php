<?php

namespace Uri;

use PHPUnit\Framework\TestCase;
use Uri\Exception\BadAuthorityException;
use Uri\Exception\BadSchemeException;
use Uri\Exception\NoHostException;
use Uri\Exception\OutOfRangePortException;

class UriTest extends TestCase
{
    public function testGetScheme(): void
    {
        $uri = new Uri();
        $this->expectException(BadSchemeException::class);
        $uri->getScheme();

        $this->expectExceptionCode('');
        $uri = $uri->setScheme('http');
        $this->assertEquals('http', $uri->getScheme());
    }

    public function testSetScheme(): void
    {
        $uri = new Uri();
        $uri = $uri->setScheme('http');
        $this->assertEquals('http', $uri->getScheme());
    }

    public function testGetUsername(): void
    {
        $uri = new Uri();
        $this->assertNull($uri->getUsername());
        $uri = $uri->setUsername('user');
        $this->assertEquals('user', $uri->getUsername());
    }

    public function testSetUsername(): void
    {
        $uri = new Uri();
        $uri = $uri->setUsername('user');
        $this->assertEquals('user', $uri->getUsername());
        $uri = $uri->setUsername(null);
        $this->assertNull($uri->getUsername());
    }

    public function testGetPassword(): void
    {
        $uri = new Uri();
        $this->assertNull($uri->getPassword());
        $uri = $uri->setPassword('pass');
        $this->assertEquals('pass', $uri->getPassword());
    }

    public function testSetPassword(): void
    {
        $uri = new Uri();
        $uri = $uri->setPassword('pass');
        $this->assertEquals('pass', $uri->getPassword());
        $uri = $uri->setPassword(null);
        $this->assertNull($uri->getPassword());
    }

    public function testGetUserInfo(): void
    {
        $uri = new Uri();

        $this->assertNull($uri->getUserInfo());
        $uri = $uri->setUsername('user');
        $this->assertEquals('user', $uri->getUserInfo());
        $uri = $uri->setPassword('password');
        $this->assertEquals('user:password', $uri->getUserInfo());
    }

    public function testSetUserInfo(): void
    {
        $uri = new Uri();

        $uri = $uri->setUserInfo(null);
        $this->assertNull($uri->getUsername());
        $this->assertNull($uri->getPassword());
        $uri = $uri->setUserInfo('user');
        $this->assertEquals('user', $uri->getUsername());
        $this->assertNull($uri->getPassword());
        $uri = $uri->setUserInfo('user:password');
        $this->assertEquals('user', $uri->getUsername());
        $this->assertEquals('password', $uri->getPassword());
    }

    public function testGetHost(): void
    {
        $uri = new Uri();
        $this->assertNull($uri->getHost());
        $uri = $uri->setHost('jsonplaceholder.typicode.com');
        $this->assertEquals('jsonplaceholder.typicode.com', $uri->getHost());
    }

    public function testSetHost(): void
    {
        $uri = new Uri();
        $uri = $uri->setHost('jsonplaceholder.typicode.com');
        $this->assertEquals('jsonplaceholder.typicode.com', $uri->getHost());
        $uri = $uri->setHost(null);
        $this->assertNull($uri->getHost());
    }

    public function testGetPort(): void
    {
        $uri = new Uri();
        $this->assertNull($uri->getPort());
        $uri = $uri->setPort(1337);
        $this->assertEquals(1337, $uri->getPort());
    }

    public function testSetPort(): void
    {
        $uri = new Uri();
        $uri = $uri->setPort(1337);
        $this->assertEquals(1337, $uri->getPort());
        $uri = $uri->setPort(null);
        $this->assertNull($uri->getPort());

        $this->expectException(OutOfRangePortException::class);
        $uri->setPort(-1);
        $uri->setPort(65536);
    }

    public function testGetDestination(): void
    {
        $uri = new Uri();

        $this->assertNull($uri->getDestination());
        $uri = $uri->setHost('example.com');
        $this->assertEquals('example.com', $uri->getDestination());
        $uri = $uri->setPort(1234);
        $this->assertEquals('example.com:1234', $uri->getDestination());
    }

    public function testSetDestination(): void
    {
        $uri = new Uri();

        $uri = $uri->setDestination(null);
        $this->assertNull($uri->getHost());
        $this->assertNull($uri->getPort());
        $uri = $uri->setDestination('example.com');
        $this->assertEquals('example.com', $uri->getHost());
        $this->assertNull($uri->getPort());
        $uri = $uri->setDestination('example.com:1234');
        $this->assertEquals('example.com', $uri->getHost());
        $this->assertEquals(1234, $uri->getPort());
    }

    public function testHasAuthority(): void
    {
        $uri = new Uri();

        $this->assertFalse($uri->hasAuthority());
        $uri = $uri->setHost('host');
        $this->assertTrue($uri->hasAuthority());
        $uri = $uri
            ->setPort(80)
            ->setHost(null)
        ;
        $this->assertTrue($uri->hasAuthority());
        $uri = $uri
            ->setHost('host')
            ->setUsername('username')
        ;
        $this->assertTrue($uri->hasAuthority());
        $uri = $uri->setPassword('password');
        $this->assertTrue($uri->hasAuthority());
        $uri = $uri
            ->setUsername(null)
            ->setPassword(null)
            ->setHost(null)
            ->setPort(null)
        ;
        $this->assertFalse($uri->hasAuthority());
        $uri = $uri->setPath('/');
        $this->assertFalse($uri->hasAuthority());
        $uri = $uri->setScheme('https');
        $this->assertFalse($uri->hasAuthority());
        $uri = $uri->setHost('example.com');
        $this->assertTrue($uri->hasAuthority());
    }

    public function testGetAuthority(): void
    {
        $uri = new Uri();

        $this->assertNull($uri->getAuthority());
        $uri = $uri->setPort(80);
        $this->expectException(NoHostException::class);
        $uri->getAuthority();
        $this->expectException(null);
        $uri = $uri->setHost('example.com');
        $this->assertEquals('//example.com:80', $uri->getAuthority());
        $uri = $uri->setUsername('username');
        $this->assertEquals('//username@example.com:80', $uri->getAuthority());
        $uri = $uri->setPassword('password');
        $this->assertEquals('//username:password@example.com:80', $uri->getAuthority());
        $uri = $uri->setPort(null);
        $this->assertEquals('//username:password@example.com', $uri->getAuthority());
        $uri = $uri->setPassword(null);
        $this->assertEquals('//username@example.com', $uri->getAuthority());
    }

    public function testSetAuthority(): void
    {
        $uri = new Uri();

        $this->expectException(BadAuthorityException::class);
        $uri = $uri->setAuthority('bad');

        $this->expectException(null);
        $uri = $uri->setAuthority('//example.com');
        $this->assertEquals('example.com', $uri->getHost());
        $this->assertNull($uri->getPort());
        $this->assertNull($uri->getUsername());
        $this->assertNull($uri->getPassword());

        $uri = $uri->setAuthority('//username@example.com');
        $this->assertEquals('example.com', $uri->getHost());
        $this->assertNull($uri->getPort());
        $this->assertEquals('username', $uri->getUsername());
        $this->assertNull($uri->getPassword());

        $uri = $uri->setAuthorty('//username@example.com:443');
        $this->assertEquals('example.com', $uri->getHost());
        $this->assertEquals(443, $uri->getPort());
        $this->assertEquals('username', $uri->getUsername());
        $this->assertNull($uri->getPassword());

        $uri = $uri->setAuthority('//username:password@example.com:443');
        $this->assertEquals('example.com', $uri->getHost());
        $this->assertEquals(443, $uri->getPort());
        $this->assertEquals('username', $uri->getUsername());
        $this->assertEquals('password', $uri->getPassword());
    }

    public function testGetPath(): void
    {
        $uri = new Uri();
        $this->assertEquals('', $uri->getPath());
        $uri = $uri->setPath('/posts/1');
        $this->assertEquals('/posts/1', $uri->getPath());
    }

    public function testSetPath(): void
    {
        $uri = new Uri();
        $uri = $uri->setPath('/posts/1');
        $this->assertEquals('/posts/1', $uri->getPath());
    }

    public function testGetQuery(): void
    {
        $uri = new Uri();
        $this->assertEquals(null, $uri->getQuery());
        $uri = $uri->setQuery('?parameter=value');
        $this->assertEquals('?parameter=value', $uri->getQuery());
    }

    public function testSetQuery(): void
    {
        $uri = new Uri();
        $uri = $uri->setQuery('?parameter=value');
        $this->assertEquals('?parameter=value', $uri->getQuery());
    }

    public function testGetFragment(): void
    {
        $uri = new Uri();
        $uri = $uri->setFragment('#id');
        $this->assertEquals('#id', $uri->getFragment());
        $uri = $uri->setFragment(null);
        $this->assertNull($uri->getFragment());
    }

    public function testSetFragment(): void
    {
        $uri = new Uri();
        $uri = $uri->setFragment('#id');
        $this->assertEquals('#id', $uri->getFragment());
        $uri = $uri->setFragment(null);
        $this->assertNull($uri->getFragment());
    }

    public function testIsValid(): void
    {
        $uri = new Uri();

        $this->assertFalse($uri->isValid());
        $uri = $uri->setScheme('http');
        $this->assertTrue($uri->isValid());
        $uri = $uri->setPath('/path');
        $this->assertTrue($uri->isValid());
        $uri = $uri->setPort(12);
        $this->assertFalse($uri->isValid());
        $uri = $uri->setHost('example.com');
        $this->assertTrue($uri->isValid());
        $uri = $uri->setPassword('password');
        $this->assertFalse($uri->isValid());
        $uri = $uri->setUsername('username');
        $this->assertTrue($uri->isValid());
    }

    public function test__toString(): void
    {
        $uri = new Uri();
        $this->expectException(BadSchemeException::class);
        $uri->__toString();

        $this->expectException(null);

        $uri = $uri->setScheme('https');
        $this->assertEquals('https:', $uri->__toString());

        $uri = $uri->setHost('jsonplaceholder.typicode.com');
        $this->assertEquals('https://jsonplaceholder.typicode.com', $uri->__toString());

        $uri = $uri->setPort(8000);
        $this->assertEquals('https://jsonplaceholder.typicode.com:8000', $uri->__toString());

        $uri = $uri->setPath('/posts/1');
        $this->assertEquals('https://jsonplaceholder.typicode.com:8000/posts/1', $uri->__toString());

        $uri = $uri->setUsername('user');
        $this->assertEquals('https://user@jsonplaceholder.typicode.com:8000/posts/1', $uri->__toString());

        $uri = $uri->setPassword('pass');
        $this->assertEquals('https://user:pass@jsonplaceholder.typicode.com:8000/posts/1', $uri->__toString());

        $uri = $uri->setUsername(null);
        $this->assertEquals('https://jsonplaceholder.typicode.com:8000/posts/1', $uri->__toString());

        $uri = $uri->setQuery('?key=value');
        $this->assertEquals('https://jsonplaceholder.typicode.com:8000/posts/1?key=value', $uri->__toString());

        $uri = $uri->setQuery('?key=value&another=12');
        $this->assertEquals('https://jsonplaceholder.typicode.com:8000/posts/1?key=value&another=12', $uri->__toString());

        $uri = $uri->setQuery('?key=spaced+value');
        $this->assertEquals('https://jsonplaceholder.typicode.com:8000/posts/1?key=spaced+value', $uri->__toString());

        $uri = $uri->setFragment('#identifier');
        $this->assertEquals('https://jsonplaceholder.typicode.com:8000/posts/1?key=spaced+value#identifier', $uri->__toString());

        $uri = new Uri();
        $uri = $uri->setPath('/my/path');
        $this->assertEquals('/my/path', $uri->__toString());

        $uri = $uri->setPort(54);
        $this->expectException(NoHostException::class);
        $uri->__toString();

        $uri = $uri->setPort(null);
        $uri = $uri->setUsername('username');
        $uri->__toString();
    }

    public function testEscape(): void
    {
        $unescaped = '#!?';
        $escaped = '%23%21%3F';

        $this->assertEquals($escaped, Uri::escape($unescaped));
    }

    public function testUnescape(): void
    {
        $unescaped = '#!?';
        $escaped = '%23%21%3F';

        $this->assertEquals($unescaped, Uri::unescape($escaped));
    }

    public function testParse(): void
    {
        $uri = Uri::parse('https://john.doe@www.example.com:123/forum/questions/?tag=networking&order=newest#top');

        $this->assertEquals('https', $uri->getScheme());
        $this->assertEquals('john.doe', $uri->getUsername());
        $this->assertEquals('john.doe', $uri->getUserInfo());
        $this->assertEquals('www.example.com', $uri->getHost());
        $this->assertEquals('//john.doe@www.example.com:123', $uri->getAuthority());
        $this->assertEquals(123, $uri->getPort());
        $this->assertEquals('/forum/questions/', $uri->getPath());
        $this->assertEquals('?tag=networking&order=newest', $uri->getQuery());
        $this->assertEquals('#top', $uri->getFragment());

        $uri = Uri::parse('https://john.doe:password@www.example.com:123/forum/questions/?tag=networking&order=newest#top');

        $this->assertEquals('https', $uri->getScheme());
        $this->assertEquals('john.doe', $uri->getUsername());
        $this->assertEquals('password', $uri->getPassword());
        $this->assertEquals('john.doe:password', $uri->getUserInfo());
        $this->assertEquals('www.example.com', $uri->getHost());
        $this->assertEquals('//john.doe:password@www.example.com:123', $uri->getAuthority());
        $this->assertEquals(123, $uri->getPort());
        $this->assertEquals('/forum/questions/', $uri->getPath());
        $this->assertEquals('?tag=networking&order=newest', $uri->getQuery());
        $this->assertEquals('#top', $uri->getFragment());

        $uri = Uri::parse('ldap://[2001:db8::7]/c=GB?objectClass?one');

        $this->assertEquals('ldap', $uri->getScheme());
        $this->assertEquals('//[2001:db8::7]', $uri->getAuthority());
        $this->assertEquals('[2001:db8::7]', $uri->getHost());
        $this->assertEquals('/c=GB', $uri->getPath());
        $this->assertEquals('?objectClass?one', $uri->getQuery());

        $uri = Uri::parse('mailto:John.Doe@example.com');

        $this->assertEquals('mailto', $uri->getScheme());
        $this->assertEquals('John.Doe@example.com', $uri->getPath());

        $uri = Uri::parse('news:comp.infosystems.www.servers.unix');

        $this->assertEquals('news', $uri->getScheme());
        $this->assertEquals('comp.infosystems.www.servers.unix', $uri->getPath());

        $uri = Uri::parse('tel:+1-816-555-1212');

        $this->assertEquals('tel', $uri->getScheme());
        $this->assertEquals('+1-816-555-1212', $uri->getPath());

        $uri = Uri::parse('telnet://192.0.2.16:80/');

        $this->assertEquals('telnet', $uri->getScheme());
        $this->assertEquals('192.0.2.16:80', $uri->getDestination());
        $this->assertEquals('192.0.2.16', $uri->getHost());
        $this->assertEquals(80, $uri->getPort());
        $this->assertEquals('/', $uri->getPath());

        $uri = Uri::parse('urn:oasis:names:specification:docbook:dtd:xml:4.1.2');

        $this->assertEquals('urn', $uri->getScheme());
        $this->assertEquals('oasis:names:specification:docbook:dtd:xml:4.1.2', $uri->getPath());

        $uri = Uri::parse('https://example.com');

        $this->assertEquals('https', $uri->getScheme());
        $this->assertEquals('example.com', $uri->getHost());
    }
}
