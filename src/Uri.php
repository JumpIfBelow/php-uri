<?php

namespace Uri;

use Uri\Exception\BadAuthorityException;
use Uri\Exception\BadFragmentException;
use Uri\Exception\BadPathException;
use Uri\Exception\BadQueryException;
use Uri\Exception\BadSchemeException;
use Uri\Exception\NoHostException;
use Uri\Exception\OutOfRangePortException;

class Uri
{
    /**
     * @var string $scheme Name of the scheme, like SSH, FTP, HTTP...
     */
    private $scheme;

    /**
     * @var string|null $username The username to identify by
     */
    private $username;

    /**
     * @var string|null $password The password to identify with
     */
    private $password;

    /**
     * @var string|null $host Could be an IP or a domain
     */
    private $host;

    /**
     * @var int|null $port Port of the connection, like 80 for web, 1337 for leet
     */
    private $port;

    /**
     * @var string $path The path part, right after the host:port.
     */
    private $path;

    /**
     * @var string|null $query The query part, like "?key=value&otherkey=othervalue;next=follow"
     */
    private $query;

    /**
     * @var string|null $fragment The fragment identifier, at the end of the URI: "#fragment"
     */
    private $fragment;

    public function __construct()
    {
        $this->path = '';
    }

    /**
     * @return string
     * @throws BadSchemeException
     */
    public function getScheme(): string
    {
        if (strlen($this->scheme) === 0) {
            throw new BadSchemeException('An URI must have a non-empty scheme.');
        }

        return $this->scheme;
    }

    /**
     * @param string $scheme
     * @return static
     * @throws BadSchemeException
     */
    public function setScheme(string $scheme): self
    {
        if (!$scheme) {
            throw new BadSchemeException('An URI must have a non-empty scheme.');
        }

        $uri = clone $this;
        $uri->scheme = $scheme;

        return $uri;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     * @return static
     */
    public function setUsername(?string $username): self
    {
        $uri = clone $this;
        $uri->username = strlen($username) ? $username : null;

        return $uri;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return static
     */
    public function setPassword(?string $password): self
    {
        $uri = clone $this;
        $uri->password = $password;

        return $uri;
    }

    /**
     * @return string
     */
    public function getUserInfo(): ?string
    {
        $userInfo = $this->username;

        if (strlen($this->password)) {
            $userInfo .= ':' . $this->password;
        }

        return $userInfo;
    }

    /**
     * @param string|null $userInfo
     * @return static
     */
    public function setUserInfo(?string $userInfo): self
    {
        $uri = clone $this;
        preg_match('/^(.+?)(?::(.+))?$/', $userInfo, $matches);

        return $uri
            ->setUsername($matches[1] ?? null)
            ->setPassword($matches[2] ?? null)
        ;
    }

    /**
     * @return string|null
     */
    public function getHost(): ?string
    {
        return $this->host;
    }

    /**
     * @param string|null $host
     * @return static
     */
    public function setHost(?string $host): self
    {
        $uri = clone $this;
        $uri->host = strlen($host) ? $host : null;

        return $uri;
    }

    /**
     * @return int|null
     */
    public function getPort(): ?int
    {
        return $this->port;
    }

    /**
     * @param int|null $port
     * @return static
     * @throws OutOfRangePortException
     */
    public function setPort(?int $port): self
    {
        if ($port < 0 || $port > 65535) {
            throw new OutOfRangePortException('A port can only be between 0 and 65535 inclusive.');
        }

        $uri = clone $this;
        $uri->port = $port;

        return $uri;
    }

    /**
     * Note: "destination" is not part of the RFC. It basically contains host[:port].
     * @return string
     */
    public function getDestination(): ?string
    {
        $destination = $this->host;

        if ($this->port) {
            $destination .= ':' . $this->port;
        }

        return $destination;
    }

    /**
     * Note: "destination" is not part of the RFC. It basically contains host[:port].
     * @param string $destination
     * @return static
     * @throws OutOfRangePortException
     */
    public function setDestination(?string $destination): self
    {
        $uri = clone $this;

        preg_match('/^(.+?)(?::(\d+))?$/', $destination, $matches);

        return $uri
            ->setHost($matches[1] ?? null)
            ->setPort($matches[2] ?? null)
        ;
    }

    /**
     * @return bool
     */
    public function hasAuthority(): bool
    {
        return $this->host !== null
            || $this->port !== null
            || $this->username !== null
            || $this->password !== null
        ;
    }

    /**
     * @return string|null
     * @throws NoHostException
     */
    public function getAuthority(): ?string
    {
        if (!$this->hasAuthority()) {
            return null;
        }

        // if there is an authority, the host is mandatory
        if (!strlen($this->host)) {
            throw new NoHostException();
        }

        $authority = '//';
        $userInfo = $this->getUserInfo();

        if (strlen($userInfo)) {
            $authority .= $userInfo . '@';
        }

        $authority .= $this->getDestination();

        return $authority;
    }

    /**
     * @param string $authority
     * @return static
     * @throws BadAuthorityException
     * @throws OutOfRangePortException
     */
    public function setAuthority(?string $authority): self
    {
        $uri = clone $this;

        if (!$authority) {
            return $uri
                ->setUserInfo(null)
                ->setDestination(null)
            ;
        }

        if (strpos($authority, '//') !== 0) {
            throw new BadAuthorityException('An authority must start with "//".');
        }

        $remaining = substr($authority, 2);
        $parts = explode('@', $remaining);

        if (array_key_exists(1, $parts)) {
            $uri = $uri->setUserInfo($parts[0]);
            $remaining = $parts[1];
        } else {
            $remaining = $parts[0];
        }

        $uri = $uri->setDestination($remaining);

        return $uri;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return static
     * @throws BadPathException
     */
    public function setPath(string $path): self
    {
        if (strlen($path) > 0) {
            if ($this->hasAuthority()) {
                if (strpos($path, '/') !== 0) {
                    throw new BadPathException('A path on an URI with an authority must start with "/".');
                }
            } else {
                if (strpos($path, '//') === 0) {
                    throw new BadPathException('A path without an authority cannot start with "//" as it would be confusing between an authority and an empty first segment.');
                }
            }
        }

        $uri = clone $this;
        $uri->path = $path;

        return $uri;
    }

    /**
     * @return string|null
     */
    public function getQuery(): ?string
    {
        return $this->query;
    }

    /**
     * @param string|null $query
     * @return static
     * @throws BadQueryException
     */
    public function setQuery(?string $query): self
    {
        if (strlen($query) && strpos($query, '?') !== 0) {
            throw new BadQueryException('The query must start with "?".');
        }

        $uri = clone $this;
        $uri->query = strlen($query) ? $query : null;

        return $uri;
    }

    /**
     * @return string|null
     */
    public function getFragment(): ?string
    {
        return $this->fragment;
    }

    /**
     * @param string|null $fragment
     * @return static
     * @throws BadFragmentException
     */
    public function setFragment(?string $fragment): self
    {
        if (strlen($fragment) && strpos($fragment, '#') !== 0) {
            throw new BadFragmentException('The fragment must start with "#".');
        }

        $uri = clone $this;
        $uri->fragment = strlen($fragment) ? $fragment : null;

        return $uri;
    }

    /**
     * Checks if the actual URI is a valid one.
     * Useful because some combination cannot be done in an URI, but it cannot be checked in setter directly.
     * It would be blocking otherwise.
     * @return bool
     */
    public function isValid(): bool
    {
        if (!strlen($this->scheme)) {
            return false;
        }

        if ($this->hasAuthority()) {
            if (!strlen($this->host)) {
                return false;
            }
        }

        if ($this->port !== null) {
            if (!strlen($this->host)) {
                return false;
            }
        }

        if ($this->password !== null) {
            if (!strlen($this->username)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return string
     * @throws NoHostException
     * @throws BadSchemeException
     */
    public function __toString(): string
    {
        if (!strlen($this->scheme)) {
            throw new BadSchemeException('An URI must have a non-empty scheme.');
        }

        return $this->scheme
            . ':'
            . $this->getAuthority()
            . $this->path
            . $this->query
            . $this->fragment
        ;
    }

    /**
     * @param string|null $value
     * @return string
     * @see urlencode()
     */
    public static function escape(?string $value): string
    {
        return urlencode($value);
    }

    /**
     * @param string|null $value
     * @return string
     * @see urldecode()
     */
    public static function unescape(?string $value): string
    {
        return urldecode($value);
    }

    /**
     * Parse the URI string to get a fully completed object.
     * @param string $uri
     * @return static
     * @throws BadAuthorityException
     * @throws BadFragmentException
     * @throws BadPathException
     * @throws BadQueryException
     * @throws BadSchemeException
     * @throws OutOfRangePortException
     */
    public static function parse(string $uri): self
    {
        $parsedUri = new Uri();
        $pieces = explode(':', $uri, 2);

        if (array_key_exists(1, $pieces)) {
            $remaining = $pieces[1];
            $parsedUri = $parsedUri->setScheme($pieces[0]);
        } else {
            $remaining = $pieces[0];
        }

        $hasAuthority = strpos($remaining, '//') === 0;

        if ($hasAuthority) {
            $remaining = substr($remaining, 2);
            $pieces = explode('/', $remaining, 2);

            $parsedUri = $parsedUri->setAuthority('//' . $pieces[0]);

            if (array_key_exists(1, $pieces)) {
                $remaining = '/' . $pieces[1];
            } else {
                $remaining = '';
            }
        }

        $pieces = explode('#', $remaining, 2);

        if (array_key_exists(1, $pieces)) {
            $parsedUri = $parsedUri->setFragment('#' . $pieces[1]);
        }

        $remaining = $pieces[0];
        $pieces = explode('?', $remaining, 2);

        if (array_key_exists(1, $pieces)) {
            $parsedUri = $parsedUri->setQuery('?' . $pieces[1]);
        }

        $remaining = $pieces[0];
        $parsedUri = $parsedUri->setPath($remaining);

        return $parsedUri;
    }
}
