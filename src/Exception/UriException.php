<?php

namespace Uri\Exception;

/**
 * Class UriException is the base exception for the package.
 * @package Uri\Exception
 */
abstract class UriException extends \Exception
{
}
