<?php

namespace Uri\Exception;

class BadFragmentException extends UriException
{
}
