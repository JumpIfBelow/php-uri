<?php

namespace Uri\Exception;

class BadSchemeException extends UriException
{
}
