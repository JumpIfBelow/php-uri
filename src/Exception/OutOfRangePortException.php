<?php

namespace Uri\Exception;

class OutOfRangePortException extends UriException
{
}
