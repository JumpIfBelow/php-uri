<?php

namespace Uri\Exception;

/**
 * Class NoHostException is thrown when a host is not set in an URI, but must be set.
 * @package Uri\Exception
 */
class NoHostException extends UriException
{
}
